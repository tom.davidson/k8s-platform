#!/usr/bin/env bash
shopt -s extglob

# Lowercased, shortened to 63 bytes, and with everything except 0-9 and a-z
# replaced with -. No leading / trailing -. Use in URLs, host names and domain names.
#
# slugify $string
slugify () {
  next=${1//+([^A-Za-z0-9])/-}
  next=${next:0:63}
  next=${next,,}
  next=${next#-}
  next=${next%-}
  echo "$next"
}

err_msg_backend="backend config is required"
err_msg_event="event or cmd is required"
err_msg_input="input allows values of true|false"
err_msg_plan="plan_file must have a 'tfplan' extention"
err_msg_module="module is required"

f_MODULES="find -mindepth 2 -not -path "*.terraform*" -name '*.tf' -printf '%P\n' \
            | xargs -I % dirname % | sort -f -u"

# Applies the provided tf plan. plan_file is required.
#
# apply plan_file input
apply () {
  local plan_file="$1"
  local input="${2:-false}"

  if [[ ! "$input" =~ ^(true|false)$ ]]; then echo "$err_msg_input"; exit 1; fi
  if [[ ! "$plan_file" =~ (.tfplan) ]]; then echo "$err_msg_plan"; exit 1; fi

  local cmd="terraform apply -input=$input $plan_file"
  echo "$cmd"
  eval "$cmd"
}

clean () {
local auto=${1:-false}

  if [[ ! $auto ]]
  then
    local cmd="rm -f *.tfplan-* && rm -r -f .terraform"
  else
    local cmd="rm -I *.tfplan-* && rm -I -r .terraform"
  fi
  echo "$cmd"
  eval "$cmd" || exit 0
}

# Checks all the .tf files if formating is needed. Fails if write is needed.
#
# format
format () {
  if [[ $(terraform fmt --write=false) ]]
  then
      echo "¬ style"
      terraform fmt --diff=true
      exit 1
  else
      echo "√ style"
  fi
}

# Downloads modules with out initializing a backend. Maybe useful for seeding
# ci cache
#
# get

get () {
  local modules=${1:-"all"}
  if [[ $modules =~ ^(all)$ ]]; then
    modules=$(eval "$f_MODULES")
  fi

  local update=${2:-"true"}
  if [[ $update =~ ^(update|true)$ ]]; then update="-update"; fi


  for m in $modules; do
    terraform get $update "$m" &
  done
  wait;
}

# Initialize terraform - module and backend are required.
#
# init module(s) backend input
init () {
  local module=${1?err_msg_module}
  local backend="${2?$err_msg_backend}"
  local input="${3:-false}"

  # if backed config is not explicit, assume partial config of S3 backend with
  # the same name of bucket and lock table
  if [[ ! "$backend" =~ (-backend-config=|-backend=false) ]]
  then
    backend="-backend-config="bucket=$backend" -backend-config="dynamodb_table=$backend""
  fi

  if [[ ! "$input" =~ ^(true|false)$ ]]; then echo "$err_msg_input"; exit 1; fi

  local cmd
  cmd="terraform init $backend $module $input"
  echo "$cmd"
  eval "$cmd"
}


# Create a tf plan - module and plan_file are requied
#
# plan module plan_file input destroy
plan () {
  local module=${1?$err_msg_module}
  local plan_file="-out=$2"
  local input="${3:-false}"
  local destroy
  destroy=$([[ "$4" = "destroy" ]] && echo -destroy || echo '' )

  if [[ ! $plan_file =~ (.tfplan) ]]; then echo "$err_msg_plan"; exit 1; fi

  if [[ ! $input =~ ^(true|false)$ ]]; then echo "$err_msg_input"; exit 1; fi

  local cmd="terraform plan $destroy $plan_file -input=$input $module"
  echo "$cmd"
  eval "$cmd"
}

# Finds all tf modules and `tf validate` on each. Pass 'false' to skip
# checking the variables
#
# validate false module(s)
validate () (
  local check_var=${1:-true}
  local modules=${2:-""}

  if [[ -z "$2" ]]; then
    modules=$(eval "$f_MODULES")
  fi

  cmd () {
    local name=${1/src|./${PWD##*/}}

    if [[ $(terraform validate -check-variables="$2" "$1") ]]
    then
      echo "¬ validate $name"
      exit 1;
    else
      echo "√ validate $name";
    fi
  }

  for m in $modules; do cmd "$m" "$check_var" & done
  wait;
)

COMMIT_REF=$(git symbolic-ref --short HEAD)
COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG:-$(slugify "$COMMIT_REF")}
TF_WORKSPACE=${TF_WORKSPACE:-${PWD##*/}}
TF_ENV=${TF_ENV:-$COMMIT_REF_SLUG}

cmd () {
  local event
  if [[ -z "$npm_lifecycle_event" ]]; then
    event=${1?$err_msg_event}
    shift
  else
    event=$npm_lifecycle_event
  fi

  local module=${2}
  local backend=${TF_BACKEND:-""}
  local automate
  automate=$([[ -z $CI || "$CI" =~ (false)  ]] && echo false || echo true)
  local plan_file
  plan_file=$([[ $event =~ (plan) ]] && echo "${TF_WORKSPACE}.tfplan" || echo null)

  if ! [[ $event =~ (format|lint|clean|get) ]];
  then
    : "${module?$err_msg_module}"
    init "$module" "$backend" "$automate"
  fi

  case $event in
    *"init"*  )
      ;;
    *"apply"* )
      apply "$plan_file" "$automate"
      ;;
    *"clean"*  )
      clean "$automate"
      ;;
    *"destroy"* )
      plan "$module" "$plan_file" "$automate" destroy
      apply "$plan_file" "$automate"
      ;;
    *"format"* )
      format
      ;;
    *"get"* )
      get
      ;;
    *"lint"* )
      format
      validate false
      ;;
    *"plan"* )
      plan "$module" "$plan_file" "$automate"
      ;;
    * )
      echo usage
      ;;
  esac
}
cmd "$@"
