#!/bin/bash

set -euo pipefail
#IFS=$'\n\t'

# Tests if docker image exists by trying a pull for 5 seconds.
# Hacky but the docker cli does not provide
# [[ $(image_exist debian:stable) == "true" ]] && echo yep || echo nope
# isIMAGE=$([[ -z $(timeout 5s docker pull "$CI_RUNNER_IMAGE_NAME:$IMAGE_TAG") ] && echo false || else echo true); echo $isIMAGE
image_exist () {
  local image=${1?"Image name is needed."}
  if [[ -z $(timeout 5s docker pull "$image") ]]; then
    echo false
  else
    echo true
  fi
}

# Determines the best common answers between two commit refs (master and feature)
# and checks glob for diffs.
# [[ $(build_diff files) ]] && echo yep || echo nope
build_diff () {
  local patn
  patn=${1:-"."}

  # convert names to hash  local target="master"
  local target
  target=${2:-"master"}
  local target_ref
  target_ref=$(git show-ref -s --heads "$target" || echo "$target")
  local source
  source=${3:-$(git rev-parse --verify HEAD)}
  local source_ref
  source_ref=$(git show-ref -s --heads "$source" || echo "$source")

  # when target and source are the same (post integration), use the branch's
  # parent as ancestor
  # When they are not the same, find the 'best' common node as ancestor
  local ancestor
  if [[ $target_ref == "$source_ref" ]]; then
    ancestor=$(git rev-parse --verify "$target_ref"^)
  else
    ancestor=$(git merge-base --all "$target_ref" "$source_ref" || git merge-base --fork-point "$target_ref" "$source_ref")
  fi

  local diffs
  diffs=$(git diff --name-only "$ancestor".."$source_ref" -- $patn)

  if [[ -z ${diffs//[[:space:]]/} ]]; then
    echo false
  else
    echo true
  fi
}
