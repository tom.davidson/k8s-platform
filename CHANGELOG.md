# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.4.1"></a>
## [0.4.1](https://gitlab.com/cloud-config-stack/k8s-platform/compare/v0.4.0...v0.4.1) (2017-10-28)



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/cloud-config-stack/k8s-platform/compare/v0.2.0...v0.4.0) (2017-10-28)


### Features

* **addon:** chaining mod to avoid races on local files ([3980917](https://gitlab.com/cloud-config-stack/k8s-platform/commit/3980917))
* **core:** impliment creds from s3 bucket ([84f8ab0](https://gitlab.com/cloud-config-stack/k8s-platform/commit/84f8ab0))



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/cloud-config-stack/k8s-platform/compare/v0.2.0...v0.3.0) (2017-10-28)


### Features

* **addon:** chaining mod to avoid races on local files ([3980917](https://gitlab.com/cloud-config-stack/k8s-platform/commit/3980917))
* **core:** impliment creds from s3 bucket ([84f8ab0](https://gitlab.com/cloud-config-stack/k8s-platform/commit/84f8ab0))



<a name="0.2.0"></a>
# 0.2.0 (2017-10-26)


### Features

* **core:** booyah - initial commit ([ddaa757](https://gitlab.com/cloud-config-stack/k8s-platform/commit/ddaa757))
