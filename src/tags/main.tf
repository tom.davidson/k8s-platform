/**
 * Combines classes of tags and validates against a schema.
 *
 * Usage:
 *
 *    module "tags" {
 *      source = ""
 *      default = {
 *        name = "${var.name}"
 *      }
 *
 *      required = {
 *        data-sensitivity = "super secret"
 *      }
 *
 *      extra = "${var.tags}"
 *    }
 *
 *    locals {
 *      tags = "${module.tags.map}"
 *      deploy_ready = "${module.tags.is_compliant && true}"
 *    }
 *
 */

variable "default" {
  default     = {}
  description = "A map of default tags - furthest left in the merge. (optional)"
}

variable "extra" {
  default     = {}
  description = "A map of tags that override defaults - typically module input. (optional)"
}

variable "required" {
  default     = {}
  description = "A map of tags that overide all others. (optional)"
}

variable "schema" {
  default = [
    "data-sensitivity",
    "description",
    "environment",
    "name",
    "usage",
    "vcs-ref",
    "vcs-url",
    "vendor",
    "version",
  ]

  description = <<EOF
A list of required tags to verify non-empty values against.
Defaults to 'Resource Schema':
  [
    "data-sensitivity",
    "description",
    "environment",
    "name",
    "usage",
    "vcs-ref",
    "vcs-url",
    "vendor",
    "version",
  ]
EOF
}

output "is_compliant" {
  value       = "${local.is_compliant}"
  description = "Returns true if all schema keys have values."
}

output "map" {
  value       = "${local.map}"
  description = "A map of combined tags."
}

output "flat" {
  value       = "${local.flat}"
  description = "Combined tags flattened to a string."
}

output "schema" {
  value       = "${local.schema}"
  description = "The schema used to validate is_compliant."
}

locals {
  is_compliant = "${length(local.schemaed_keys) < length(local.schema) ? false : true}"
  map          = "${merge(var.default, local.ccs_defaults, var.extra, var.required)}"
  flat         = "${join(",", keys(local.map))} | ${join(",", values(local.map))}"

  resource_schema = [
    "data-sensitivity",
    "description",
    "environment",
    "name",
    "usage",
    "vcs-ref",
    "vcs-url",
    "vendor",
    "version",
  ]

  ccs_defaults = {
    provisioner = "terraform"
    workspace   = "${terraform.workspace}"
    vendor      = "ccs"
  }

  schema = "${split(",", length(var.schema) > 0 
                        ? join(",", var.schema) 
                        : join(",", local.resource_schema)
                      )}"

  schemaed_keys = "${compact(matchkeys(values(local.map), keys(local.map), var.schema))}"
}
