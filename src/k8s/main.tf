/**
 * Provisions a K8S Platform cluster on with Tectonic.
 *
 * Usage:
 *
 *    module "ccsk8s" {
 *      source =
 *    }
 *
 */

locals {
  module_name = "k8s"
}

module "tectonic-installer" {
  source = "github.com/coreos/tectonic-installer.git//platforms/aws?ref=1.7.5"

  tectonic_admin_email                    = "${lookup(local.tectonic, "email")}"
  tectonic_admin_password_hash            = "${lookup(local.tectonic, "password_hash")}"
  tectonic_aws_etcd_ec2_type              = "${lookup(local.etcd, "aws_ec2_type")}"
  tectonic_aws_etcd_extra_sg_ids          = "${local.aws_etcd_extra_sg_ids}"
  tectonic_aws_etcd_root_volume_iops      = "${lookup(local.etcd, "aws_root_volume_iops")}"
  tectonic_aws_etcd_root_volume_size      = "${lookup(local.etcd, "aws_root_volume_size")}"
  tectonic_aws_etcd_root_volume_type      = "${lookup(local.etcd, "aws_root_volume_type")}"
  tectonic_aws_external_master_subnet_ids = "${local.aws_external_master_subnet_ids}"
  tectonic_aws_external_private_zone      = "${lookup(local.network, "aws_external_private_zone")}"
  tectonic_aws_external_vpc_id            = "${lookup(local.network, "aws_external_vpc_id")}"
  tectonic_aws_external_worker_subnet_ids = "${local.aws_external_worker_subnet_ids}"
  tectonic_aws_extra_tags                 = "${local.tags}"
  tectonic_aws_master_ec2_type            = "${lookup(local.master, "aws_ec2_type")}"
  tectonic_aws_master_extra_sg_ids        = "${local.aws_master_extra_sg_ids}"
  tectonic_aws_master_iam_role_name       = "${lookup(local.master, "aws_iam_role_name")}"
  tectonic_aws_master_root_volume_iops    = "${lookup(local.master, "aws_root_volume_iops")}"
  tectonic_aws_master_root_volume_size    = "${lookup(local.master, "aws_root_volume_size")}"
  tectonic_aws_master_root_volume_type    = "${lookup(local.master, "aws_root_volume_type")}"
  tectonic_aws_public_endpoints           = true
  tectonic_aws_region                     = "${lookup(local.cluster, "aws_region")}"
  tectonic_aws_ssh_key                    = "${lookup(local.iam, "aws_key_pair_name")}"
  tectonic_aws_vpc_cidr_block             = "${lookup(local.network, "aws_vpc_cidr_block")}"
  tectonic_aws_worker_ec2_type            = "${lookup(local.worker, "aws_ec2_type")}"
  tectonic_aws_worker_extra_sg_ids        = "${local.aws_worker_extra_sg_ids}"
  tectonic_aws_worker_iam_role_name       = "${lookup(local.worker, "aws_iam_role_name")}"
  tectonic_aws_worker_root_volume_iops    = "${lookup(local.worker, "aws_root_volume_iops")}"
  tectonic_aws_worker_root_volume_size    = "${lookup(local.worker, "aws_root_volume_size")}"
  tectonic_aws_worker_root_volume_type    = "${lookup(local.worker, "aws_root_volume_type")}"
  tectonic_base_domain                    = "${lookup(local.dns, "base_zone")}"
  tectonic_cluster_name                   = "${lookup(local.cluster, "name")}"
  tectonic_dns_name                       = "${lookup(local.dns, "prefix")}"
  tectonic_etcd_count                     = "${lookup(local.etcd, "node_count")}"
  tectonic_license_path                   = "${module.wait_for_license.filename}"
  tectonic_master_count                   = "${lookup(local.master, "node_count")}"
  tectonic_pull_secret_path               = "${module.wait_for_pull_secret.filename}"
  tectonic_vanilla_k8s                    = "${lookup(local.tectonic, "vanilla_k8s")}"
  tectonic_worker_count                   = "${lookup(local.worker, "node_count")}"
}

module "wait_for_pull_secret" {
  source   = "../wait-for-file"
  filename = "${lookup(local.tectonic, "pull_secret_path")}"
}

module "wait_for_license" {
  source   = "../wait-for-file"
  filename = "${lookup(local.tectonic, "license_path")}"
}
