resource "tls_private_key" "default" {
  algorithm = "RSA"
}

resource "aws_key_pair" "default" {
  key_name   = "${local.name}"
  public_key = "${tls_private_key.default.public_key_openssh}"
}

data "aws_s3_bucket_object" "admin_email" {
  bucket = "${lookup(var.tectonic, "secret_bucket")}"
  key    = "${lookup(var.tectonic, "admin_email")}"
  count  = "${lookup(var.tectonic, "secret_bucket", "") != "" ? 1 : 0}"
}

data "aws_s3_bucket_object" "admin_password_hash" {
  bucket = "${lookup(var.tectonic, "secret_bucket")}"
  key    = "${lookup(var.tectonic, "admin_password_hash")}"
  count  = "${lookup(var.tectonic, "secret_bucket", "") != "" ? 1 : 0}"
}

data "aws_s3_bucket_object" "license" {
  bucket = "${lookup(var.tectonic, "secret_bucket")}"
  key    = "${lookup(var.tectonic, "license_path")}"
  count  = "${lookup(var.tectonic, "secret_bucket", "") != "" ? 1 : 0}"
}

resource "local_file" "license" {
  content  = "${data.aws_s3_bucket_object.license.body}"
  filename = ".terraform/tectonic/license.txt"
  count    = "${lookup(var.tectonic, "secret_bucket", "") != "" ? 1 : 0}"
}

data "aws_s3_bucket_object" "pull_secret" {
  bucket = "${lookup(var.tectonic, "secret_bucket")}"
  key    = "${lookup(var.tectonic, "pull_secret_path")}"
  count  = "${lookup(var.tectonic, "secret_bucket", "") != "" ? 1 : 0}"
}

resource "local_file" "pull_secret" {
  content  = "${data.aws_s3_bucket_object.pull_secret.body}"
  filename = ".terraform/tectonic/pull-secret.json"
  count    = "${lookup(var.tectonic, "secret_bucket", "") != "" ? 1 : 0}"
}
