output "all" {
  value = {
    aws_key_fingerprint = "${aws_key_pair.default.fingerprint}"
    aws_key_name        = "${aws_key_pair.default.key_name}"
    public_key_openssh  = "${aws_key_pair.default.public_key_openssh}"
    public_key_pem      = "${aws_key_pair.default.public_key_pem}"
  }

  description = "A map of all of not-senstive, flat outputs."
}

output "private_key_pem" {
  value       = "${aws_key_pair.default.private_key_pem}"
  sensitive   = true
  description = "The private key data in PEM format."
}

output "public_key_pem" {
  value       = "${tls_private_key.default.public_key_pem}"
  description = "The public key data in PEM format."
}

output "public_key_openssh" {
  value       = "${tls_private_key.default.public_key_openssh}"
  description = "The public key data in OpenSSH authorized_keys format."
}

output "aws_key_name" {
  value       = "${aws_key_pair.default.key_name}"
  description = "The AWS key pair name."
}

output "aws_key_fingerprint" {
  value       = "${aws_key_pair.default.fingerprint}"
  description = "The MD5 public AWS key fingerprint"
}

output "iam" {
  value       = "${local.iam}"
  description = "A map of cluster params to configure the installer"
}

output "cluster" {
  value       = "${local.cluster}"
  description = "A map of cluster params to configure the installer"
}

output "dns" {
  value       = "${local.dns}"
  description = "A map of dns params to configure the installer"
}

output "etcd" {
  value       = "${local.etcd}"
  description = "A map of etcd params to configure the installer"
}

output "network" {
  value       = "${local.network}"
  description = "A map of network params to configure the installer"
}

output "master" {
  value       = "${local.master}"
  description = "A map of master params to configure the installer"
}

output "worker" {
  value       = "${local.worker}"
  description = "A map of worker params to configure the installer"
}

output "tags" {
  value       = "${local.tags}"
  description = "A map of tags uses for K8S"
}
