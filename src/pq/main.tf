/**
 * Creates an IAM Role and policy with the minimal required access for the
 * CCS K8S Installer
 *
 */

locals {
  module_name = "pq"
}

resource "aws_iam_role" "default" {
  name               = "${local.name}"
  path               = "${lookup(local.iam, "installer_role_path")}"
  assume_role_policy = "${data.template_file.trust_policy.rendered}"
}

data "template_file" "trust_policy" {
  template = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com",
        "AWS": "arn:aws:iam::$${account_id}:root"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  vars {
    account_id = "${local.account_id}"
  }
}

resource "aws_iam_role_policy" "default" {
  name_prefix = "${local.name}"
  role        = "${aws_iam_role.default.id}"
  policy      = "${data.template_file.iam_policy.rendered}"
}

# https://coreos.com/tectonic/docs/latest/files/aws-policy.json
data "template_file" "iam_policy" {
  template = "${file("${path.module}/policy.json")}"
}

/**
 * DNS related resources for the CCS K8S Installer
 *
 */

data "aws_route53_zone" "parent" {
  name = "${lookup(local.dns, "parent_zone")}"
}

resource "aws_route53_zone" "base" {
  comment = "${local.comment}"
  name    = "${lookup(local.dns, "base_zone")}"
  tags    = "${local.tags}"
}

resource "aws_route53_record" "base_ns" {
  zone_id = "${data.aws_route53_zone.parent.zone_id}"
  name    = "${lookup(local.dns, "base_zone")}"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.base.name_servers.0}",
    "${aws_route53_zone.base.name_servers.1}",
    "${aws_route53_zone.base.name_servers.2}",
    "${aws_route53_zone.base.name_servers.3}",
  ]
}
