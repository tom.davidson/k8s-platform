output "all" {
  value = {
    base_zone_id = "${aws_route53_zone.base.zone_id}"
    role_arn     = "${aws_iam_role.default.arn}"
  }

  description = "A map of all of not-senstive, flat outputs."
}

output "base_zone_id" {
  value       = "${aws_route53_zone.base.zone_id}"
  description = "The base DNS domain zone_id for the cluster."
}

output "role_arn" {
  description = "The ARN assigned by AWS to this role."
  value       = "${aws_iam_role.default.arn}"
}
