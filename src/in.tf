variable "names" {
  default = {}

  description = <<EOF
Name of the named resources, seperated as a map to exploit unqiue nameing
features.
{
  root   = ""
  prefix = ""
  suffix = "" // suggest setting to env name (prod, dev, etc...)
}
EOF
}

variable "tags" {
  default = {}

  description = <<EOF
Tags to be applied to created cloud resources.
(optional)
EOF
}

variable "iam" {
  default = {}

  description = <<EOF
Various IAM resources for the K8S installer. Defaults are listed:
{
  aws_key_pair_name = ""
  Name of an SSH key located within the AWS region. If unset a new one will be
  created. The private key is stored in the Terraform plan's state.
  Note: This input is currently ignored.
}
EOF
}

variable "tectonic" {
  default = {}

  description = <<EOF
Parameters to configure Tectonis specific services on the cluster.
Defaults are listed:
{
  admin_email = ""
  The e-mail address used to login as the admin user to the Tectonic Console.

  admin_password_hash = ""
  admin_password = ""
  The admin user password to login to the Tectonic Console. Set one. Both the plain text
  and the hash are stored in the state (which should already be encrypted at
  rest and in transport.
  Use the bcrypt-hash tool (https://github.com/coreos/bcrypt-tool/)

  license_path = ""
  The path to the tectonic licence file.

  pull_secret_path = ""
  The path to the tectonic pull secret config.

  secret_bucket = ""
  The AWS S3 Bucket that stores the license and pull_secret files. When set, the
  paths aretreated as object names and prefixes rather than local files.

  Note: If the license or pull_secret are not set, then
      `tectonic_vanilla_k8s` is set to `true`.
}
EOF
}

variable "cluster" {
  default = {}

  description = <<EOF
Parameters to configure the k8s cluster. Defaults are listed:
{
  name = prefix-root-suffix
  The name of the cluster.
  This will be prepended to `base_zone` resulting in the URL to the Tectonic
  console.
  Note: This field MUST be set manually prior to creating the cluster.
  Warning: Special characters in the name like '.' may cause errors.

  public_facing = true

  region = 'us-west-2'
}
EOF
}

variable "dns" {
  default = {}

  description = <<EOF
{
  base_zone = ""
  The base DNS domain of the cluster.
  Note: This field MUST be set manually prior to creating the cluster.

  dns_name_prefix = ""
  DNS prefix used to construct the console and API server endpoints.
  (optional)
}
EOF
}

variable "etcd" {
  default = {}

  description = <<EOF
Parameters to configure the k8s cluster's etcd service. Defaults are listed:
{
  aws_etcd_ec2_type  = "t2.medium"
  Instance size for the etcd node(s).

  aws_etcd_extra_sg_ids = []
  List of additional security group IDs for etcd nodes.
  Example: `["sg-51530134", "sg-b253d7cc"]`
  (optional)

  aws_etcd_root_volume_type =   = "gp2"
  The type of volume for the root block device of etcd nodes.

  aws_etcd_root_volume_size = "30"
  The size of the volume in gigabytes for the root block device of etcd nodes.

  aws_etcd_root_volume_iops = "100"
  The amount of provisioned IOPS for the root block device of etcd nodes.

}
EOF
}

variable "network" {
  default = {}

  description = <<EOF
Parameters to configure the k8s cluster's network. Defaults are listed:
{
  aws_external_private_zone = ""
  If set, the given Route53 zone ID will be used as the internal (private) zone.
  This zone will be used to create etcd DNS records as well as internal API and
  internal Ingress records. If set, no additional private zone will be created.
  Example: `"Z1ILINNUJGTAO1"`
  (optional)

  aws_external_vpc_id = ""
  ID of an existing VPC to launch nodes into. If unset a new VPC is created.
  Example: `vpc-123456`
  (optional)

  aws_external_vpc_public = cluster.public_facing
  If set to true, create public facing ingress resources (ELB, A-records).
  If set to false, a "private" cluster will be created with an internal ELB only.

  aws_master_custom_subnets = {}
  This configures master availability zones and their corresponding subnet CIDRs
  directly.
  Example: `{ eu-west-1a = "10.0.0.0/20", eu-west-1b = "10.0.16.0/20" }`
  (optional)

  aws_vpc_cidr_block = "10.0.0.0/16"
  Block of IP addresses used by the VPC. This should not overlap with any peered
  or VPN'd networks, such as a private datacenter connected via Direct Connect.

  aws_worker_custom_subnets = {}
  This configures worker availability zones and their corresponding subnet CIDRs
  directly.
  Example: `{ eu-west-1a = "10.0.64.0/20", eu-west-1b = "10.0.80.0/20" }`
  (optional)
}
EOF
}

variable "master" {
  default = {}

  description = <<EOF
Parameters to configure the k8s cluster's master node(s). Defaults are listed:
{
  aws_master_ec2_type = "t2.medium"
  Instance size for the master node(s).

  aws_master_extra_sg_ids = []
  List of additional security group IDs for master nodes.
  Example: `["sg-51530134", "sg-b253d7cc"]`
  (optional)

  aws_master_iam_role_name = ""
  Name of IAM role to use for the instance profiles of master nodes. The name is
  also the last part of a role's ARN.
  Example:
  * Role ARN  = arn:aws:iam::123456789012:role/ccsk8s-installer
  * Role Name = ccsk8s-installer
  (optional)

  aws_master_root_volume_type = "gp2"
  The type of volume for the root block device of master nodes.

  aws_master_root_volume_size = "30"
  The size of the volume in gigabytes for the root block device of master nodes.

  aws_master_root_volume_iops = "100"
  The amount of provisioned IOPS for the root block device of master nodes.

  node_count = 1
  The number of nodes to be created.
}
EOF
}

variable "worker" {
  default = {}

  description = <<EOF
Parameters to configure the k8s cluster's worker node(s). Defaults are listed:
{
  aws_worker_ec2_type = "t2.medium"
  Instance size for the worker node(s).

  aws_worker_extra_sg_ids = []
  List of additional security group IDs for worker nodes.
  Example: `["sg-51530134", "sg-b253d7cc"]`
  (optional)

  aws_worker_iam_role_name = ""
  Name of IAM role to use for the instance profiles of worker nodes. The name is
  also the last part of a role's ARN.
  Example:
  * Role ARN  = arn:aws:iam::123456789012:role/ccsk8s-installer
  * Role Name = ccsk8s-installer
  (optional)

  aws_worker_root_volume_type = "gp2"
  The type of volume for the root block device of worker nodes.

  aws_worker_root_volume_size = "30"
  The size of the volume in gigabytes for the root block device of worker nodes.

  aws_worker_root_volume_iops = "100"
  The amount of provisioned IOPS for the root block device of worker nodes.

  node_count = 1
  The number of nodes to be created.
}
EOF
}
