#!/usr/bin/env bash

set -euo pipefail

filename=$1
timeout=$2

wait_file() {
  local file="$1"; shift
  local wait_seconds="${1:-10}"; shift
  local count=0
  local exists="false"

  while [[ $exists != "true" && $count -lt $wait_seconds ]] ; do
    [ -f "$file" ] && exists="true" || exists="false"
    let count+=1
    sleep 1
  done

  printf '{"filename":"%s","exists":"%s","wait":"%s"}\n' "$filename" "$exists" "$count"
  exit
}

wait_file "$filename" $timeout
