variable "filename" {}

variable "enable" {
  default = "true"
}

variable "timeout" {
  default     = 15
  description = "The maximum number of seconds to wait for the file."
}

locals {
  filename = "${pathexpand(var.filename)}"
  timeout  = "${var.enable ? var.timeout : 1}"

  default = {
    filename = "${local.filename}"
    exists   = "false"
    wait     = ""
  }

  results = "${data.external.wait_for_file.result}"
}

data "external" "wait_for_file" {
  program = ["bash", "${path.module}/wait_for_file.sh", "${local.filename}", "${local.timeout}"]
}

output "results" {
  value = "${local.results}"
}

output "filename" {
  value = "${lookup(local.results, "filename")}"
}
