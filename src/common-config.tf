/**
 * Defaults and other config items for the CCS K8S Installer
 *
 */

terraform {
  required_version = ">= 0.10.7"
}

data "aws_caller_identity" "current" {}

locals {
  account_id = "${data.aws_caller_identity.current.account_id}"

  cluster_defaults = {
    aws_region    = "us-west-2"
    name          = "${local.name}"
    public_facing = true
  }

  cluster = "${merge(local.cluster_defaults, var.cluster)}"

  comment = "Managed by ccs/k8s-platform for ${lookup(local.tags, "vendor", "" )}: ${lookup(local.tags, "env", "" )}/${lookup(local.tags, "workspace", "" )}"

  dns_defaults = {
    prefix      = ""
    base_zone   = "${lookup(var.dns, "base_zone", "")}"
    hostname    = "${join(".", slice(local.dns_split, 0, 1))}"
    parent_zone = "${join(".", slice(local.dns_split, 1, length(local.dns_split)))}"
  }

  dns_split = "${split(".", lookup(var.dns, "base_zone", ""))}"

  dns = "${merge(local.dns_defaults, var.dns)}"

  etcd_defaults = {
    aws_ec2_type         = "t2.medium"
    aws_root_volume_iops = "100"
    aws_root_volume_size = "30"
    aws_root_volume_type = "gp2"
  }

  aws_etcd_extra_sg_ids = []

  etcd = "${merge(local.etcd_defaults, var.etcd)}"

  iam_defaults = {
    installer_role_path = "/"
  }

  iam_required = {
    aws_key_pair_name = "${element(concat(aws_key_pair.default.*.key_name, list("")), 0)}"
  }

  iam = "${merge(local.iam_defaults, var.iam, local.iam_required)}"

  name = "${join("-", compact(list(
                  lookup(var.names, "prefix", ""),
                  lookup(var.names, "root", ""),
                  lookup(var.names, "suffix", ""),
          )))}"

  network_defaults = {
    aws_external_private_zone = ""
    aws_external_vpc_id       = ""
    aws_external_vpc_public   = "${lookup(local.cluster, "public_facing", true)}"
    aws_vpc_cidr_block        = "10.0.0.0/16"
  }

  aws_external_master_subnet_ids = [""]
  aws_external_worker_subnet_ids = [""]

  network = "${merge(local.network_defaults, var.network)}"

  master_defaults = {
    aws_ec2_type         = "t2.medium"
    aws_iam_role_name    = ""
    aws_root_volume_iops = "100"
    aws_root_volume_size = "30"
    aws_root_volume_type = "gp2"
    node_count           = 1
  }

  aws_master_extra_sg_ids = []
  master                  = "${merge(local.master_defaults, var.master)}"

  tags = "${module.tags.map}"

  tags_default = {
    name = "${local.name}"

    #   vendor           = "ccs/k8s-platform"
    data-sensitivity = "confidential"
  }

  tags_flat     = "${module.tags.flat}"
  tags_required = {}

  tectonic = {
    email = "${trimspace(element(concat(
              data.aws_s3_bucket_object.admin_email.*.body,
              list(lookup(var.tectonic, "admin_email", ""))),
              0))}"

    password_hash = "${trimspace(element(concat(
              data.aws_s3_bucket_object.admin_password_hash.*.body,
              list(lookup(var.tectonic, "admin_password_hash", "")),
              list(bcrypt(lookup(var.tectonic, "admin_password", "")))),
              0))}"

    license_path = "${element(concat(
              local_file.license.*.filename,
              list(lookup(var.tectonic, "license_path", ""))),
              0)}"

    pull_secret_path = "${element(concat(
              local_file.pull_secret.*.filename,
              list(lookup(var.tectonic, "pull_secret_path", ""))),
              0)}"

    vanilla_k8s = "${lookup(var.tectonic, "license_path", "") == ""
              || lookup(var.tectonic, "pull_secret_path", "") == ""
              ? true : false}"
  }

  worker_defaults = {
    aws_ec2_type         = "t2.medium"
    aws_iam_role_name    = ""
    aws_root_volume_iops = "100"
    aws_root_volume_size = "30"
    aws_root_volume_type = "gp2"
    node_count           = 1
  }

  aws_worker_extra_sg_ids = []
  worker                  = "${merge(local.worker_defaults, var.worker)}"
}

module "tags" {
  source = "../tags"

  default  = "${local.tags_default}"
  extra    = "${var.tags}"
  required = "${local.tags_required}"
}
